import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { View } from 'react-native';
import Home from './src/pages/Main';

export default function App() {
  return (
    <>
      <StatusBar style="light" backgroundColor="transparent" />
      <Home/>
    </>
  );
}

