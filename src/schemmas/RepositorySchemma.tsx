export default class RepositorySchema {
    static Schema:Realm.ObjectSchema = {
        name: 'Repository',
        primaryKey: 'id',
        properties: {
            id: { type: 'int', indexed: true },
            name: 'string',
            description: 'string',
            stars: 'int',
            forks: 'int'
        },
    };
}