import Realm from 'realm';

import RepositorySchema from '../schemmas/RepositorySchemma';

export default function getRealm(){
    return Realm.open({
        schema: [{
            name: 'Repository',
            primaryKey: 'id',
            properties: {
                id: { type: 'int', indexed: true },
                name: 'string',
                description: 'string',
                stars: 'int',
                forks: 'int'
            },
        }],
    })
}