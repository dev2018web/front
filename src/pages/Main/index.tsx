import React, {useState} from 'react';
import { Container, Title, Form, Input, Submit, List } from './styles';
import Repository from '../components/Repository';
import Icon from 'react-native-vector-icons/MaterialIcons';

import api from '../../services/api';
import getRealm from '../../services/realm';

interface RepositoryData {
    id: string,
    name: string,
    full_name: string,
    description: string,
    startgazers_count: number,
    forks_count: number
}

export default function Main(){
    const [input, setInput] = useState();

    async function saveRepository(repository:RepositoryData){
        const data = {
            id: repository.id,
            name: repository.name,
            fullName: repository.full_name,
            description: repository.description,
            stars: repository.startgazers_count,
            forks: repository.forks_count
        }

        const realm = await getRealm();
    }

    async function handleAddRepository(){

        try{
            const response = await api.get(`repos${input}`);

            console.log(response);
        }catch(err){
        console.log(err);

        }
    }

    return(
        <Container>
            <Title>Repositórios</Title>

            <Form>
                <Input
                    value={input}
                    onChangeText={(text:any) => setInput(text)}
                    autoCapitalize="none"
                    autoCorrect={false}
                    />
                <Submit onPress={handleAddRepository}>
                    <Icon name="add" size={22} color="#FFF"/>
                </Submit>
            </Form>

            <List 
                keyboardShouldPersistTaps="handled"
                data={[{
                    id: 1,
                    name: 'unform',
                    description: "Lorem ipsum dolor sit amet consectetur",
                    stars: 1234,
                    forks: 1212
                }]}
                keyExtractor={(item:any) => String(item.id)}
                renderItem={({ item }) => <Repository data={item}/>}
                />
        </Container>
    );
}

