import React from 'react';
import { Name, Description, Stats, Stat, Container, StartCount } from "./styles";
import Icon from 'react-native-vector-icons/FontAwesome';

export default function Home(dados:any) {
    const { data } = dados;

    return (
        <Container>
            <Name>{data?.name}</Name>
            <Description>{data?.description}</Description>

            <Stats>
                <Stat>
                    <Icon name="star" size={16} color="#333" />
                    <StartCount>{data?.stars}</StartCount>
                </Stat>
                <Stat>
                    <Icon name="code-fork" size={16} color="#333" />
                    <StartCount>{data?.stars}</StartCount>
                </Stat>
            </Stats>
        </Container>
    );
}

